# sample-consumer-sink

## About the project 
This is a sample maven project and contains an example of consuming data from IDS Datastreaming topics.It uses Spring boot, Apache Camel and kafka-connect-jdbc to move data from a kafka topic to table in postgresql database.

## Project configuration
Before running application, following properties need to be set in application.properties(/src/main/resources/application.properties) file.

### Kafka broker configuration 
Below properties control the way in which spring boot application connects to kafka broker endpoints. 
````
kafka.brokers=kafka.us-west-2.dev.stream.idscloud.io:9092
kafka.topic=stage.<custname>.canonical.as_equip_desc_nf,stage.<custname>.canonical.ls_billing_nf
kafka.group-id=<custname>-group-1
kafka.client-id=<custname>-1
kafka.consumers-count=1
kafka.security-protocol=SSL
kafka.ssl-truststore-location=D:/kafka_2.12-2.7.0/ssl/client_certs/truststore.jks
kafka.ssl-truststore-password=***************
kafka.ssl-keystore-location=D:/kafka_2.12-2.7.0/ssl/client_certs/keystore.jks
kafka.ssl-keystore-password=***************
kafka.ssl-key-password=***************
kafka.schema-registry-u-r-l=https://schema-0.us-west-2.dev.stream.idscloud.io:8081
kafka.heartbeat-interval-ms=3000
kafka.auto-offset-reset=earliest
kafka.specific-avro-reader=true

### below two properties are required when using SASL_SSL security protocol to connect to kafka brokers. SASL MECHANISM is set to PLAIN.
kafka.saslJaasConfig=org.apache.kafka.common.security.plain.PlainLoginModule required username=UserName password=SecretPassword;
kafka.schema-registry-user-config=schema-registry-username:schema-registry-password

### If you want to create database tables using the avro schema before starting to consume the topics then
### Set kafka.create.db.tables.at.start to true. By default it is set to false
### Get avro schemas, save it locally and pass filepath to application using kafka.local.schema.repository
kafka.local.schema.repository=path to local repo
kafka.create.db.tables.at.start=false
````

One must correctly set below properties when using **SSL** security protocol- 
1. kafka.security-protocol
2. kafka.ssl-truststore-location
3. kafka.ssl-truststore-password
4. kafka.ssl-keystore-location
5. kafka.ssl-keystore-password
6. kafka.ssl-key-password.

to point the application to correct ssl certificates. More information can be found at - https://docs.confluent.io/platform/current/installation/configuration/consumer-configs.html

### Connecting to kafka broker hosted in Confluent Cloud
Before starting, get a service account set up in confluent cloud and receive separate api keys for kafka cluster and schema registry. The api key should also contain secret password.
One must correctly set below properties- 
1. kafka.security-protocol
2. kafka.saslJaasConfig
3. kafka.schema-registry-user-config

**SASL_SSL** protocol is used to connect to kafka cluster hosted in Confluent Cloud and more information can be found at - https://docs.confluent.io/platform/current/kafka/authentication_sasl/index.html .
kafka.saslJaasConfig and kafka.schema-registry-user-config properties are used to pass jass configuration to this application

### Kafka Jdbc-Sink Connector configuration
Application uses kafka-connect-jdbc to create database tables and upsert data into them. Therefore its required to modify following properties in
application.properties file.
````
kafka.connector.auto.create=true
kafka.connector.auto.evolve=true
kafka.connector.insert.mode=upsert
kafka.connector.delete.enabled=true
kafka.connector.pk.mode=record_key
kafka.connector.retryBackoffMs=3000
````  
more information about these properties can be found at - https://docs.confluent.io/kafka-connect-jdbc/current/sink-connector/sink_config_options.html

### Database connection configuration
Following properties are required to configure Postgresql database connection properly:
````
spring.datasource.url=jdbc:postgresql://hostName:5432/testdb
spring.datasource.username=nishant
spring.datasource.password=****************
````
As mentioned previously, this application uses **io.confluent.connect.jdbc.sink.JdbcSinkTask.java** internally to create tables for topics and writing to them. Therefore any dialect mentioned in https://github.com/confluentinc/kafka-connect-jdbc/tree/master/src/main/java/io/confluent/connect/jdbc/dialect can be used as database, if one provides proper Kafka Jdbc-Sink Connector configuration and Database connection configuration.

### Consuming Topics
Initialize "kafka.topic" property in application.properties with a comma separted string containing the names of topics to consume. The application will create consumers for each topic mentioned and start reading from them. The data is the routed to Postgresql database which was defined in databse connection above.

## Custom Route
Heart of an apache camel applications is **route**. These are configuration which define - how data will flow between a source-destination pair. 
Any transformation required in between is also defined here. See GenericByteArrayConsumer.java for example. One can create similar class extending "RouteBuilder.java" to create custom routes.

## Running the application 
On Intellij:
1. modify application.properties to point to correct broker and database.
2. select **Edit Configurations** of intellij and make sure option - **include dependencies with "Provided" scope**, is selected.

## Building jar
One can run maven "package" goal via intellij or command line to create a jar file of the project. After finishing execution, one should get  "ids-datastreaming-1.0-SNAPSHOT.jar" and "ids-datastreaming-1.0-SNAPSHOT-spring-boot.jar" files in target folder.

## Downloading latest jar from pipeline
One can download zipped artifacts from last successful pipeline run - https://gitlab.com/ids-inc/idscloud/datamart/sample-consumer-sink/-/pipelines.

### Running the jar files
Before running make sure application.properties file is in the same folder as the above jar files. On command prompt or terminal, give following command to start the application -
````bash
java -jar ids-datastreaming-1.0-SNAPSHOT-spring-boot.jar
````
Please make sure, the ssl paths are correct.
