package com.ids.datamart.avro.transformations;

import org.apache.kafka.connect.connector.ConnectRecord;
import org.apache.kafka.connect.data.ConnectSchema;
import org.apache.kafka.connect.data.Field;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.SchemaBuilder;
import org.apache.kafka.connect.sink.SinkRecord;
import org.apache.kafka.connect.transforms.Transformation;
import org.apache.kafka.connect.transforms.util.SchemaUtil;

import java.util.List;

public class TopicMessageTransformers {

    private static final String INT_8 = "int8";

    private static final String INT_16 = "int16";

    private static final String INT_32 = "int32";

    private static final String INT_64 = "int64";

    private static final String FLOAT_32 = "float32";

    private static final String FLOAT_64 = "float64";

    private static final String BOOLEAN = "boolean";

    private static final String STRING = "string";

    private String topicName;

    private List<MessageTransformation<? extends Transformation>> messageTransformations;

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public void addMessageTransformations(MessageTransformation<? extends Transformation> messageTransformation) {
        messageTransformations.add(messageTransformation);
    }

    public void setTransformations(List<MessageTransformation<? extends Transformation>> messageTransformations) {
        this.messageTransformations = messageTransformations;
    }

    public ConnectRecord<SinkRecord> executeTransformations(ConnectRecord<SinkRecord> sinkRecord, DatabaseType databaseType) {
        if (topicName.equals(sinkRecord.topic())) {
            for (MessageTransformation<? extends Transformation> messageTransformation : messageTransformations) {
                if (messageTransformation.getDatabaseType().equals(databaseType)) {
                    sinkRecord = messageTransformation.getTransformation().apply(sinkRecord);
                }
            }
        } else {
            throw new UnsupportedOperationException(String.format("These transformations are not allowed for %s topic", sinkRecord.topic()));
        }
        return sinkRecord;
    }

    public List<MessageTransformation<? extends Transformation>> getMessageTransformations() {
        return messageTransformations;
    }

    public boolean containsTransformationForField(String fieldName){
        for(MessageTransformation<? extends Transformation> messageTransformation: messageTransformations){
            if(messageTransformation.containsTransformationForField(fieldName)){
                return true;
            }
        }
        return false;
    }

    public String getTransformationForField(String fieldName){
        for(MessageTransformation<? extends Transformation> messageTransformation: messageTransformations){
            if(messageTransformation.containsTransformationForField(fieldName)){
                return messageTransformation.getTransformationForField(fieldName);
            }
        }
        return null;
    }

    private Schema getFieldSchemaForTransformation(String transformationType){
        Schema schema;
        switch(transformationType){
            case INT_8:
                schema = Schema.INT8_SCHEMA;
                break;
            case INT_16:
                schema = Schema.INT16_SCHEMA;
                break;
            case INT_32:
                schema = Schema.INT32_SCHEMA;
                break;
            case INT_64:
                schema = Schema.INT64_SCHEMA;
                break;
            case FLOAT_32:
                schema = Schema.FLOAT32_SCHEMA;
                break;
            case FLOAT_64:
                schema = Schema.FLOAT64_SCHEMA;
                break;
            case BOOLEAN:
                schema = Schema.BOOLEAN_SCHEMA;
                break;
            case STRING :
                schema = Schema.STRING_SCHEMA;
                break;
            default:
                throw new UnsupportedOperationException(String.format("Could not find schema for this transformation %s", transformationType));
        }
        return schema;
    }

    public Schema updateSchema(Schema schema){
        SchemaBuilder builder = SchemaUtil.copySchemaBasics(schema, SchemaBuilder.struct());
        for (Field field : schema.fields()) {
            if(containsTransformationForField(field.name())){
                String transformationType = getTransformationForField(field.name());
                if(transformationType!=null) {
                    Schema newFieldSchema = getFieldSchemaForTransformation(transformationType);
                    Schema fieldSchema = field.schema();
                    Schema updateSchema = new ConnectSchema(newFieldSchema.type(), fieldSchema.isOptional(), fieldSchema.defaultValue(), newFieldSchema.name(),
                            fieldSchema.version(), fieldSchema.doc());
                    builder.field(field.name(), updateSchema);
                }
            }else{
                builder.field(field.name(), field.schema());
            }
        }
        return builder.build();
    }
}
