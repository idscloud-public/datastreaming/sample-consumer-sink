package com.ids.datamart.avro.transformations;

import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.sink.SinkRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageTransformer {

    @Autowired
    private SqlServerTransformations sqlServerTransformations;

    public SinkRecord applyTransformation(String topic, DatabaseType databaseType, SinkRecord sinkRecord) {
        TopicMessageTransformers topicMessageTransformers = getTopicMessageTransformers(topic, databaseType);
        if (topicMessageTransformers != null) {
            sinkRecord = (SinkRecord) topicMessageTransformers.executeTransformations(sinkRecord, databaseType);
        }
        return sinkRecord;
    }

    public TopicMessageTransformers getTopicMessageTransformers(String topic, DatabaseType databaseType){
        TopicMessageTransformers topicMessageTransformers = null;
        switch (databaseType) {
            case SQL_SERVER:
                topicMessageTransformers = sqlServerTransformations.getTopicMessageTransformers(topic);
                break;
            default:
                break;
        }
        return topicMessageTransformers;
    }


    public Schema updateSchemaWithTransformation(String topic, Schema schema, DatabaseType databaseType){
        TopicMessageTransformers topicMessageTransformers = getTopicMessageTransformers(topic, databaseType);
        if(topicMessageTransformers!=null){
            schema = topicMessageTransformers.updateSchema(schema);
        }

        return schema;
    }
}
