package com.ids.datamart.avro.transformations;

import org.apache.kafka.connect.sink.SinkRecord;
import org.apache.kafka.connect.transforms.Cast;
import org.apache.kafka.connect.transforms.Transformation;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SqlServerTransformations {

    private static final String USER_DEFINED_DATA_TOPIC = "user_defined_data";

    private static final String ACTIVITY_DETAIL_TOPIC = "activity_detail";

    private static final String LS_BILLING_NF_TOPIC = "ls_billing_nf";

    private static final String CS_MASTER_NF_TOPIC = "cs_master_nf";

    private static final String DATAMART_CONTRACT_TOPIC = "datamart_contract";

    private Map<String, TopicMessageTransformers> topicMessageTransformersMap;

    private void addCastTransformationToMap(String topic, Map<String, Object> config) {
        Cast<SinkRecord> cast = new Cast.Value<>();
        cast.configure(config);
        List<MessageTransformation<? extends Transformation>> messageTransformationList = new ArrayList<>();
        MessageTransformation messageTransformation = new MessageTransformation(cast, DatabaseType.SQL_SERVER, config);
        messageTransformationList.add(messageTransformation);

        TopicMessageTransformers topicMessageTransformers = new TopicMessageTransformers();
        topicMessageTransformers.setTransformations(messageTransformationList);
        topicMessageTransformers.setTopicName(topic);

        topicMessageTransformersMap.put(topic, topicMessageTransformers);
    }

    private void addLsBillingNfTopicTransformationsToMap() {
        Map<String, Object> config = new HashMap<>();
        config.put("spec", "billing_tmpl:int64");
        addCastTransformationToMap(LS_BILLING_NF_TOPIC, config);
    }

    private void addCsMasterNfTopicTransformationsToMap() {
        Map<String, Object> config = new HashMap<>();
        config.put("spec", "cust_ap_vendor:int64");
        addCastTransformationToMap(CS_MASTER_NF_TOPIC, config);
    }

    private void addDatamartContractTransformationToMap() {
        Map<String, Object> config = new HashMap<>();
        config.put("spec", "billing_tmpl:int64");
        addCastTransformationToMap(DATAMART_CONTRACT_TOPIC, config);
    }

    private void addUserDefinedDataTransformationToMap() {
        Map<String, Object> config = new HashMap<>();
        config.put("spec", "integer_val:int64");
        addCastTransformationToMap(USER_DEFINED_DATA_TOPIC, config);
    }

    private void addActivityDetailTransformationToMap() {
        Map<String, Object> config = new HashMap<>();
        config.put("spec", "id:int64");
        addCastTransformationToMap(ACTIVITY_DETAIL_TOPIC, config);
    }


    @PostConstruct
    public void init() {
        topicMessageTransformersMap = new HashMap<>();
        addLsBillingNfTopicTransformationsToMap();
        addCsMasterNfTopicTransformationsToMap();
        addDatamartContractTransformationToMap();
        addUserDefinedDataTransformationToMap();
        addActivityDetailTransformationToMap();
    }

    public TopicMessageTransformers getTopicMessageTransformers(String topic) {
        return topicMessageTransformersMap.get(topic);
    }
}
