package com.ids.datamart.avro.transformations;

public enum DatabaseType {
    SQL_SERVER,
    POSTGRESQL
}
