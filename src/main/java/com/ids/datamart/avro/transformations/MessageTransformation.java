package com.ids.datamart.avro.transformations;

import org.apache.kafka.connect.transforms.Transformation;

import java.util.HashMap;
import java.util.Map;


public class MessageTransformation<T extends Transformation> {

    private T transformation;

    private DatabaseType databaseType;

    private Map<String, Object> config;

    private Map<String, String> fieldTransformationMap;


    public MessageTransformation(T transformation, DatabaseType databaseType, Map<String, Object> config) {
        this.transformation = transformation;
        this.databaseType = databaseType;
        this.config = config;
    }

    public T getTransformation() {
        return transformation;
    }

    public DatabaseType getDatabaseType() {
        return databaseType;
    }

    public Map<String, Object> getConfig(){
        return config;
    }


    private Map<String, String> parseConfig(){
        if(fieldTransformationMap == null) {
            fieldTransformationMap = new HashMap<>();
            String spec = config.get("spec").toString();
            String[] transformations = spec.split(",");
            for (String transformationConfig : transformations) {
                String[] fieldTransformation = transformationConfig.split(":");
                fieldTransformationMap.put(fieldTransformation[0], fieldTransformation[1]);
            }
        }
        return fieldTransformationMap;
    }

    public boolean containsTransformationForField(String fieldName){
        if(fieldTransformationMap == null){
            parseConfig();
        }
        return fieldTransformationMap.containsKey(fieldName);
    }

    public String getTransformationForField(String fieldName){
        if(fieldTransformationMap == null){
            parseConfig();
        }

        return fieldTransformationMap.get(fieldName);
    }
}
