package com.ids.datamart.avro.processors;

import ch.qos.logback.classic.Logger;
import com.ids.datamart.avro.services.JdbcSinkTaskService;
import com.ids.datamart.avro.services.SchemaRegistryService;
import com.ids.datamart.avro.transformations.MessageTransformer;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.kafka.connect.data.*;
import org.apache.kafka.connect.sink.SinkRecord;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class GenericByteArrayProcessor implements Processor {

    public static final Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(GenericByteArrayProcessor.class);

    private SchemaRegistryService schemaRegistryService;

    private JdbcSinkTaskService jdbcSinkTaskService;

    private MessageTransformer messageTransformer;

    private int messageCount;

    public GenericByteArrayProcessor(SchemaRegistryService schemaRegistryService, JdbcSinkTaskService jdbcSinkTaskService, MessageTransformer messageTransformer) {
        this.schemaRegistryService = schemaRegistryService;
        this.jdbcSinkTaskService = jdbcSinkTaskService;
        this.messageTransformer = messageTransformer;
        messageCount = 0;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        byte[] value = exchange.getIn().getBody(byte[].class);
        byte[] key = (byte[]) exchange.getIn().getHeader("kafka.KEY");
        String topic = exchange.getIn().getHeader("kafka.TOPIC").toString();
        Integer partition = (Integer) exchange.getIn().getHeader("kafka.PARTITION");
        Long offset = (Long) exchange.getIn().getHeader("kafka.OFFSET");
        SchemaAndValue topicKeySchemaAndValue = null;
        SchemaAndValue topicValueSchemaAndValue = null;
        try {
            topicKeySchemaAndValue = schemaRegistryService.getSchemaAndValue(topic, key, true);

            if (value == null) {
                topicValueSchemaAndValue = new SchemaAndValue(null, null);
            } else {
                topicValueSchemaAndValue = schemaRegistryService.getSchemaAndValue(topic, value, false);
            }
        } catch (Exception e) {
            String stack = Arrays.toString(e.getStackTrace());
            exchange.setProperty("error", "Error connecting to schema. "+e.getMessage()+"\n" + stack);
        }

        String[] splitTopicName = topic.split("\\.");
        int length = splitTopicName.length;
        String tableName = splitTopicName[length - 1];
        SinkRecord sinkRecord = new SinkRecord(tableName, partition, topicKeySchemaAndValue.schema(), topicKeySchemaAndValue.value(), topicValueSchemaAndValue.schema(), topicValueSchemaAndValue.value(), offset);

        sinkRecord = messageTransformer.applyTransformation(tableName, jdbcSinkTaskService.getDatabaseType(), sinkRecord);

        Collection<SinkRecord> sinkRecords = new ArrayList<>();
        sinkRecords.add(sinkRecord);
        if (messageCount == 0) {
            jdbcSinkTaskService.putSinkRecordsWithSynchronization(topic, sinkRecords);
        } else {
            jdbcSinkTaskService.putSinkRecords(topic, sinkRecords);
        }
        messageCount++;
        logger.debug(String.format("successfully upserted data from topic: %s id: %s partition: %s offset: %s into table %s", topic, topicKeySchemaAndValue.value(), partition, offset, tableName));
    }
}