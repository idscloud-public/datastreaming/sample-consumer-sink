package com.ids.datamart.avro.processors;

import ch.qos.logback.classic.Logger;
import com.ids.datamart.avro.services.JdbcSinkTaskService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.SchemaBuilder;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.sink.SinkRecord;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;

public class GenericErrorHandler implements Processor {

    public static final Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(GenericErrorHandler.class);


    private JdbcSinkTaskService jdbcSinkTaskService;

    private Schema keySchema;

    private Schema valueSchema;

    private void buildKeySchema() {
        SchemaBuilder schemaBuilder = new SchemaBuilder(Schema.Type.STRUCT);
        schemaBuilder.name("unprocessed_messages_key");
        schemaBuilder.version(1);
        schemaBuilder.field("id", SchemaBuilder.string());
        keySchema = schemaBuilder.build();
    }

    private void buildValueSchema() {
        SchemaBuilder schemaBuilder = new SchemaBuilder(Schema.Type.STRUCT);
        schemaBuilder.name("unprocessed_messages_value");
        schemaBuilder.field("partition", SchemaBuilder.int32());
        schemaBuilder.field("offset", SchemaBuilder.int64());
        schemaBuilder.field("topic", SchemaBuilder.string());
        schemaBuilder.field("error", SchemaBuilder.string().optional());
        schemaBuilder.field("unprocessed_message", SchemaBuilder.bytes());
        valueSchema = schemaBuilder.build();
    }

    public GenericErrorHandler(JdbcSinkTaskService jdbcSinkTaskService) {
        this.jdbcSinkTaskService = jdbcSinkTaskService;
        buildKeySchema();
        buildValueSchema();
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        byte[] value = exchange.getIn().getBody(byte[].class);
        String topic = exchange.getIn().getHeader("kafka.TOPIC").toString();
        Integer partition = (Integer) exchange.getIn().getHeader("kafka.PARTITION");
        Long offset = (Long) exchange.getIn().getHeader("kafka.OFFSET");
        String error = (String) exchange.getProperty("error");

        logger.error(error);

        Struct valueObject = new Struct(valueSchema);
        valueObject.put("unprocessed_message", value);
        valueObject.put("partition", partition);
        valueObject.put("offset", offset);
        valueObject.put("topic", topic);
        valueObject.put("error", error);

        Struct keyObject = new Struct(keySchema);
        keyObject.put("id", topic.replace('.', '_') + "_" + partition + "_" + offset + "_" + System.currentTimeMillis());

        SinkRecord sinkRecord = new SinkRecord(jdbcSinkTaskService.getErrorTopic(), partition, keySchema, keyObject, valueSchema, valueObject, offset);
        Collection<SinkRecord> sinkRecords = new ArrayList<>();
        sinkRecords.add(sinkRecord);
        jdbcSinkTaskService.putUnprocessedRecords(sinkRecords);
    }
}
