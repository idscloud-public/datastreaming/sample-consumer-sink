package com.ids.datamart.avro.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class MessageStatisticsProcessor implements Processor {

    private Map<String, AtomicInteger> messageCountMap = new ConcurrentHashMap<>();

    public static final Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(MessageStatisticsProcessor.class);

    @Value("${kafka.topic}")
    private String topics;

    @PostConstruct
    public void init() {
        String[] topicList;
        if (topics.contains(",")) {
            topicList = topics.split(",");
        } else {
            topicList = new String[1];
            topicList[0] = topics;
        }
        for (String topic : topicList) {
            messageCountMap.put(topic, new AtomicInteger(0));
        }
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        String topic = exchange.getIn().getHeader("kafka.TOPIC").toString();
        if (messageCountMap.containsKey(topic)) {
            AtomicInteger messageCount = messageCountMap.get(topic);
            messageCount.incrementAndGet();
        }
    }

    public void printMessageStatistics() {
        for (Map.Entry<String, AtomicInteger> entry : messageCountMap.entrySet()) {
            AtomicInteger messageCount =  entry.getValue();
            String topic = entry.getKey();
            logger.info(String.format("consumed %s messages from %s topic", messageCount.get(), topic));
        }
    }
}
