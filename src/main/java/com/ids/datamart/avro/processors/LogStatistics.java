package com.ids.datamart.avro.processors;


import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class LogStatistics implements Processor {

    private MessageStatisticsProcessor messageStatisticsProcessor;

    public LogStatistics(MessageStatisticsProcessor messageStatisticsProcessor) {
        this.messageStatisticsProcessor = messageStatisticsProcessor;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        messageStatisticsProcessor.printMessageStatistics();
    }
}
