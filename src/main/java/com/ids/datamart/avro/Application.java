package com.ids.datamart.avro;

import com.ids.datamart.avro.services.JdbcSinkTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PreDestroy;


@SpringBootApplication
public class Application {

    @Autowired
    JdbcSinkTaskService jdbcSinkTaskService;

    @PreDestroy
    public void onExit() {
        jdbcSinkTaskService.shutDownAllTask();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
