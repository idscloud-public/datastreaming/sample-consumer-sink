package com.ids.datamart.avro.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumerConfigurationProvider {

    @Value("${kafka.brokers}")
    private String bootstrapServer;

    @Value("${kafka.group-id}")
    private String groupId;

    @Value("${kafka.consumers-count}")
    private String consumerCount;

    @Value("${kafka.security-protocol}")
    private String securityProtocol;

    @Value("${kafka.ssl-truststore-location:@null}")
    private String sslTruststoreLocation;

    @Value("${kafka.ssl-truststore-password:@null}")
    private String sslTruststorePassword;

    @Value("${kafka.ssl-keystore-location:@null}")
    private String sslKeystoreLocation;

    @Value("${kafka.ssl-keystore-password:@null}")
    private String sslKeystorePassword;

    @Value("${kafka.ssl-key-password:@null}")
    private String sslKeyPassword;

    @Value("${kafka.schema-registry-u-r-l}")
    private String schemaRegistryURL;

    @Value("${kafka.saslJaasConfig:@null}")
    private String saslConfiguration;


    @Value("${kafka.heartbeat-interval-ms}")
    private String heartBeatIntervalMs;

    @Value("${kafka.auto-offset-reset}")
    private String autoOffsetReset;

    public String getBasicKafkaConsumerConfiguration(String topic) {
        String configurationString;
        if(securityProtocol.equals("SSL")) {
            configurationString = "kafka:" + topic + "?" +
                    "brokers=" + bootstrapServer +
                    "&groupId=" + groupId +
                    "&autoOffsetReset=earliest" +
                    "&consumersCount=" + consumerCount +
                    "&poll-timeout-ms=60000" +
                    "&securityProtocol=" + securityProtocol +
                    "&sslTruststoreLocation=" + sslTruststoreLocation +
                    "&sslTruststorePassword=RAW(" + sslTruststorePassword + ")" +
                    "&sslKeystoreLocation=" + sslKeystoreLocation +
                    "&sslKeystorePassword=RAW(" + sslKeystorePassword + ")" +
                    "&sslKeyPassword=RAW(" + sslKeyPassword + ")" +
                    "&schemaRegistryURL=" + schemaRegistryURL +
                    "&additionalProperties.schema.registry.ssl.truststore.location=" + sslTruststoreLocation +
                    "&additionalProperties.schema.registry.ssl.truststore.password=RAW(" + sslTruststorePassword + ")" +
                    "&additionalProperties.schema.registry.ssl.keystore.location=" + sslKeystoreLocation +
                    "&additionalProperties.schema.registry.ssl.keystore.password=RAW(" + sslKeystorePassword + ")" +
                    "&heartbeatIntervalMs=" + heartBeatIntervalMs;
        } else {
            configurationString = "kafka:"+topic+"?"+
                "brokers="+ bootstrapServer +
                    "&groupId="+ groupId +
                "&autoOffsetReset=earliest"+"&consumersCount="+consumerCount+
                "&poll-timeout-ms=60000" +
                "&securityProtocol=" + securityProtocol +"&saslJaasConfig=RAW("+saslConfiguration+
                ")&saslMechanism=PLAIN"+
                "&heartbeatIntervalMs=" + heartBeatIntervalMs;
        }
        return configurationString;
    }
}
