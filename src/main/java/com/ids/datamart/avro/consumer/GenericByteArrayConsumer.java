package com.ids.datamart.avro.consumer;

import com.ids.datamart.avro.processors.GenericByteArrayProcessor;
import com.ids.datamart.avro.processors.GenericErrorHandler;
import com.ids.datamart.avro.processors.MessageStatisticsProcessor;
import com.ids.datamart.avro.services.JdbcSinkTaskService;
import com.ids.datamart.avro.services.SchemaRegistryService;
import com.ids.datamart.avro.transformations.MessageTransformer;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GenericByteArrayConsumer extends RouteBuilder {

    @Autowired
    private SchemaRegistryService schemaRegistryService;

    @Autowired
    private JdbcSinkTaskService jdbcSinkTaskService;

    @Autowired
    private KafkaConsumerConfigurationProvider kafkaConfigurationProvider;

    @Autowired
    private MessageTransformer messageTransformer;

    @Autowired
    private MessageStatisticsProcessor messageStatisticsProcessor;

    @Value("${kafka.topic}")
    private String topicsToSubscribe;

    @Override
    public void configure() throws Exception {

        List<String> topics = new ArrayList<>();
        if (topicsToSubscribe.contains(",")) {
            String[] topicList = topicsToSubscribe.split(",");
            int count = 1 ;
            String concatTopic = "";
            for(String topicName: topicList){
                if(count == 1){
                    concatTopic = topicName;
                } else {
                    concatTopic += ","+topicName;
                }
                if(count%10==0){
                    topics.add(concatTopic);
                    count = 0;
                    concatTopic = "";
                }
                count++;
            }
            if(!concatTopic.isEmpty()) {
                topics.add(concatTopic);
            }
        } else {
            topics.add(topicsToSubscribe);
        }

        onException(Exception.class).handled(true).process(new GenericErrorHandler(jdbcSinkTaskService)).maximumRedeliveries(2).maximumRedeliveryDelay(2000).useOriginalMessage();

        for (String topic : topics) {
            from(kafkaConfigurationProvider.getBasicKafkaConsumerConfiguration(topic) +
                    "&key-deserializer=org.apache.kafka.common.serialization.ByteArrayDeserializer" +
                    "&value-deserializer=org.apache.kafka.common.serialization.ByteArrayDeserializer")
                    .process(new GenericByteArrayProcessor(schemaRegistryService, jdbcSinkTaskService, messageTransformer))
                    .process(messageStatisticsProcessor);
        }
    }
}
