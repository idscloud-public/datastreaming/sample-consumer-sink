package com.ids.datamart.avro.consumer;

import com.ids.datamart.avro.processors.LogStatistics;
import com.ids.datamart.avro.processors.MessageStatisticsProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StatisticsPublisher extends RouteBuilder {

  @Autowired
  private MessageStatisticsProcessor messageStatisticsProcessor;

  @Override
  public void configure() throws Exception {
    from("timer://LogStatistics?delay=120000&period=60000").process(new LogStatistics(messageStatisticsProcessor));
  }
}
