package com.ids.datamart.avro.services;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "kafka")
public class JdbcSinkTaskConfiguration {

    private Map<String, String> connector;

    public Map<String, String> getConnector() {
        return connector;
    }

    public void setConnector(Map<String, String> connector) {
        this.connector = connector;
    }
}
