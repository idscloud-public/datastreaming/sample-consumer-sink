package com.ids.datamart.avro.services;

import org.apache.avro.Schema;

import java.util.ArrayList;
import java.util.List;

public class ConvertedSchemaInfo {

    private Schema avroSchema;
    private org.apache.kafka.connect.data.Schema convertedConnectSchema;
    private Integer version;
    private List<String> dateFieldsForCoversion;
    private boolean dateConversionRequired;

    ConvertedSchemaInfo() {
        dateFieldsForCoversion = new ArrayList<>();
        dateConversionRequired = false;
    }

    public Schema getAvroSchema() {
        return avroSchema;
    }

    public void setAvroSchema(Schema avroSchema) {
        this.avroSchema = avroSchema;
    }

    public org.apache.kafka.connect.data.Schema getConvertedConnectSchema() {
        return convertedConnectSchema;
    }

    public void setConvertedConnectSchema(org.apache.kafka.connect.data.Schema convertedSchema) {
        this.convertedConnectSchema = convertedSchema;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public void addDateFieldForConversion(String dateField) {
        if (!dateConversionRequired) {
            dateConversionRequired = true;
        }
        dateFieldsForCoversion.add(dateField);
    }

    public List<String> getDateFieldsForCoversion() {
        return dateFieldsForCoversion;
    }

    public boolean isDateConversionRequired() {
        return dateConversionRequired;
    }
}
