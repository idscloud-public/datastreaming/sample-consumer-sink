package com.ids.datamart.avro.services;

import io.confluent.connect.avro.AvroConverter;
import io.confluent.connect.avro.AvroData;
import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClientConfig;
import io.confluent.kafka.schemaregistry.client.rest.RestService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SchemaRegistryClientProvider {

    @Value("${kafka.schema-registry-u-r-l}")
    private String baseUrl;

    @Value("${kafka.ssl-truststore-location:@null}")
    private String sslTruststoreLocation;

    @Value("${kafka.ssl-truststore-password:@null}")
    private String sslTruststorePassword;

    @Value("${kafka.ssl-keystore-location:@null}")
    private String sslKeystoreLocation;

    @Value("${kafka.ssl-keystore-password:@null}")
    private String sslKeystorePassword;

    @Value("${kafka.security-protocol}")
    private String securityProtocol;


    @Value("${kafka.schema-registry-user-config:@null}")
    private String schemaRegistryUserInfoConfig;

    private SchemaRegistryClient schemaRegistryClient;

    private AvroConverter keyAvroConvertor;

    private AvroConverter valueAvroConvertor;

    private AvroData avroData;

    /**
     * Sets the ssl properties for schema registry client to use
     */
    private void setSSLProperties() {
        System.setProperty("javax.net.ssl.keyStore", sslKeystoreLocation);
        System.setProperty("javax.net.ssl.keyStorePassword", sslKeystorePassword);
        System.setProperty("javax.net.ssl.keyStoreType", "jks");
        System.setProperty("javax.net.ssl.trustStore", sslTruststoreLocation);
        System.setProperty("javax.net.ssl.trustStorePassword", sslTruststorePassword);
        System.setProperty("javax.net.ssl.trustStoreType", "jks");
    }

    /**
     * @return SchemaRegistryClient
     */
    public SchemaRegistryClient getSchemaRegistryClient() {
        if (schemaRegistryClient == null) {
            if (securityProtocol.equals("SSL")) {
                setSSLProperties();
                schemaRegistryClient = new CachedSchemaRegistryClient(baseUrl, 2000);
            } else {
                Map<String, String> map = new HashMap<>();
                map.put(SchemaRegistryClientConfig.BASIC_AUTH_CREDENTIALS_SOURCE, "USER_INFO");
                map.put(SchemaRegistryClientConfig.USER_INFO_CONFIG, schemaRegistryUserInfoConfig);
                RestService restService = new RestService(baseUrl);
                restService.configure(map);
                schemaRegistryClient = new CachedSchemaRegistryClient(restService, 2000);
            }
        }
        return schemaRegistryClient;
    }

    /**
     * @return Configuration for avro convertor {@link io.confluent.connect.avro.AvroConverter} to convert kafka value
     */
    private Map<String, Object> getValueAvroConfigrationProperties() {
        Map<String, Object> config = new HashMap<>();
        config.put("schema.registry.url", baseUrl);
        config.put("use.latest.version", true);
        return config;
    }

    /**
     * @return {@link io.confluent.connect.avro.AvroConverter} for kafka value
     */
    public AvroConverter getValueAvroConvertor() {
        if (valueAvroConvertor == null) {
            valueAvroConvertor = new AvroConverter(getSchemaRegistryClient());
            valueAvroConvertor.configure(getValueAvroConfigrationProperties(), false);
        }
        return valueAvroConvertor;
    }


    /**
     * @return Configuration for avro convertor {@link io.confluent.connect.avro.AvroConverter} to convert kafka key
     */
    private Map<String, Object> getKeyAvroConfigrationProperties() {
        Map<String, Object> config = new HashMap<>();
        config.put("schema.registry.url", baseUrl);
        return config;
    }

    /**
     * @return {@link io.confluent.connect.avro.AvroConverter} for kafka key
     */
    public AvroConverter getKeyAvroConvertor() {
        if (keyAvroConvertor == null) {
            keyAvroConvertor = new AvroConverter(getSchemaRegistryClient());
            keyAvroConvertor.configure(getKeyAvroConfigrationProperties(), true);
        }
        return keyAvroConvertor;
    }

    public AvroData getAvroData() {
        if (avroData == null) {
            avroData = new AvroData(2000);
        }
        return avroData;
    }
}
