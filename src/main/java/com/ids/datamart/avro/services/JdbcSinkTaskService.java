package com.ids.datamart.avro.services;

import com.ids.datamart.avro.transformations.DatabaseType;
import io.confluent.connect.jdbc.dialect.DatabaseDialect;
import io.confluent.connect.jdbc.dialect.DatabaseDialects;
import io.confluent.connect.jdbc.dialect.PostgreSqlDatabaseDialect;
import io.confluent.connect.jdbc.dialect.SqlServerDatabaseDialect;
import io.confluent.connect.jdbc.sink.DbStructure;
import io.confluent.connect.jdbc.sink.JdbcSinkConfig;
import io.confluent.connect.jdbc.sink.JdbcSinkTask;
import io.confluent.connect.jdbc.sink.metadata.FieldsMetadata;
import io.confluent.connect.jdbc.sink.metadata.SchemaPair;
import io.confluent.connect.jdbc.util.TableId;
import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.connect.sink.SinkRecord;
import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.ws.rs.NotFoundException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Provides methods to start and stop different {@link io.confluent.connect.jdbc.sink.JdbcSinkTask}
 */
@Service("JdbcSinkTaskService")
public class JdbcSinkTaskService {

    public static final Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(JdbcSinkTaskService.class);

    private Map<String, JdbcSinkTask> jdbcSinkTaskMap = new ConcurrentHashMap<>();

    @Autowired
    private SchemaRegistryService schemaRegistryService;

    @Value("${spring.datasource.url}")
    private String databaseConnectionUrl;

    @Value("${spring.datasource.username}")
    private String databaseUser;

    @Value("${spring.datasource.password}")
    private String databaseUserPassword;

    @Value("${kafka.connector.auto.create}")
    private String autoCreate;

    @Value("${kafka.connector.auto.evolve}")
    private String autoEvolve;

    @Value("${kafka.connector.insert.mode}")
    private String insertMode;

    @Value("${kafka.connector.delete.enabled}")
    private String deleteEnabled;

    @Value("${kafka.connector.pk.mode}")
    private String pkMode;

    @Value("${kafka.connector.retryBackoffMs}")
    private String retryBackoffMs;

    @Value("${kafka.enable.jdbc.sink.task.per.consumer:false}")
    private boolean enableJdbcSinkTaskPerConsumer;

    @Value("${kafka.topic}")
    private String topics;

    @Value("${kafka.create.db.tables.at.start:false}")
    private boolean createTablesAtStart;

    private DatabaseType databaseType;

    private static final String ERROR_TOPIC = "unprocessed_messages";

    private DatabaseDialect databaseDialect;

    @Autowired
    private JdbcSinkTaskConfiguration jdbcSinkTaskConfiguration;


    private String[] getTopicList(){
        String[] topicList;
        if (topics.contains(",")) {
            topicList = topics.split(",");
        } else {
            topicList = new String[1];
            topicList[0] = topics;
        }
        return topicList;
    }

    @PostConstruct
    public void init() {
        AbstractConfig abstractConfig = new JdbcSinkConfig(getJdbcSinkTaskConfiguration());
        databaseDialect = DatabaseDialects.findBestFor(databaseConnectionUrl, abstractConfig);
        if (databaseDialect instanceof SqlServerDatabaseDialect) {
            databaseType = DatabaseType.SQL_SERVER;
        } else if (databaseDialect instanceof PostgreSqlDatabaseDialect) {
            databaseType = DatabaseType.POSTGRESQL;
        }

        String[] topicList = getTopicList();

        if(createTablesAtStart) {
            for (String topic : topicList) {
                String[] splitTopicName = topic.split("\\.");
                int length = splitTopicName.length;
                String tableName = splitTopicName[length - 1];
                try {
                    if (!tableExits(tableName)) {
                        SchemaPair schemaPair = schemaRegistryService.getSchemaPair(topic, databaseType, tableName);
                        createTable(tableName, schemaPair);
                    }
                } catch (SQLException e) {
                    logger.error(String.format("could not create table for topic %s", topic));
                    e.printStackTrace();
                } catch (NotFoundException e) {
                    logger.error(String.format("could not get schema for topic %s", topic));
                    e.printStackTrace();
                }
            }
        }
    }


    private DatabaseDialect getDatabaseDialect(){
       return databaseDialect;
    }

    private DbStructure getDbStructure(){
        return  new DbStructure(getDatabaseDialect());
    }

    private TableId getTableId(String tableName){
        return databaseDialect.parseTableIdentifier(tableName);
    }

    private boolean tableExits(String tableName) throws SQLException {
        return databaseDialect.tableExists(databaseDialect.getConnection(), getTableId(tableName));
    }

    private void createTable(String tableName, SchemaPair schemaPair) throws SQLException{
        TableId tableId = getTableId(tableName);
        JdbcSinkConfig jdbcSinkConfig = new JdbcSinkConfig(getJdbcSinkTaskConfiguration());
        FieldsMetadata fieldsMetadata = FieldsMetadata.extract(tableName, jdbcSinkConfig.pkMode, Collections.emptyList(), Collections.emptySet(), schemaPair);
        getDbStructure().createOrAmendIfNecessary(jdbcSinkConfig, getDatabaseDialect().getConnection(), tableId, fieldsMetadata);
    }

    private Map<String, String> getJdbcSinkTaskConfiguration() {
        Map<String, String> props = new HashMap<>();
        props.put("connection.url", databaseConnectionUrl);
        props.put("connection.user", databaseUser);
        props.put("connection.password", databaseUserPassword);
        props.putAll(jdbcSinkTaskConfiguration.getConnector());

        return props;
    }

    /**
     * Jdbc Sink Task configuration
     *
     * @param topic
     * @return
     */
    private Map<String, String> getJdbcSinkTaskConfiguration(String topic) {
        Map<String, String> props = getJdbcSinkTaskConfiguration();
        props.put("name", topic);
        return props;
    }

    private Map<String, String> getErrorMessageJdbcSinkTaskConfiguration() {
        Map<String, String> props = new HashMap<>();
        props.put("name", ERROR_TOPIC);
        props.put("connection.url", databaseConnectionUrl);
        props.put("connection.user", databaseUser);
        props.put("connection.password", databaseUserPassword);
        props.put("auto.create", autoCreate);
        props.put("auto.evolve", autoEvolve);
        props.put("insert.mode", "insert");
        props.put("delete.enabled", "false");
        props.put("pk.mode", "record_key");
        props.put("retryBackoffMs", retryBackoffMs);

        return props;
    }

    private String getJdbcSinkTaskName(String topic) {
        String taskName;
        if (enableJdbcSinkTaskPerConsumer) {
            taskName = topic + Thread.currentThread().getName();
        } else {
            taskName = topic;
        }
        return taskName;
    }

    private JdbcSinkTask addTaskForTopic(String topic) {
        JdbcSinkTask jdbcSinkTask = new JdbcSinkTask();
        jdbcSinkTaskMap.put(getJdbcSinkTaskName(topic), jdbcSinkTask);
        jdbcSinkTask.start(getJdbcSinkTaskConfiguration(topic));
        return jdbcSinkTask;
    }

    /**
     * Returns a Jdbc Sink task for a particular topic
     *
     * @param topic
     * @return {@link io.confluent.connect.jdbc.sink.JdbcSinkTask}
     */
    private JdbcSinkTask getJdbcSinkTask(String topic) {
        JdbcSinkTask jdbcSinkTask = jdbcSinkTaskMap.get(getJdbcSinkTaskName(topic));
        if (jdbcSinkTask == null) {
            jdbcSinkTask = addTaskForTopic(topic);
        }
        return jdbcSinkTask;
    }

    /**
     * Stops all running jdbc sink tasks
     */
    public void shutDownAllTask() {
        for (Map.Entry<String, JdbcSinkTask> entry : jdbcSinkTaskMap.entrySet()) {
            entry.getValue().stop();
            logger.info(String.format("Stopped jdbc sink task for topic: %s", entry.getKey()));
        }
    }

    /**
     * Submits SinkRecords to JdbcSinkTask
     *
     * @param topic
     * @param records
     */
    public void putSinkRecords(String topic, Collection<SinkRecord> records) {
        getJdbcSinkTask(topic).put(records);
    }

    public synchronized void putSinkRecordsWithSynchronization(String topic, Collection<SinkRecord> records) {
        getJdbcSinkTask(topic).put(records);
    }

    /**
     * Get jdbc sink task to write error message
     *
     * @return
     */
    private JdbcSinkTask getUnprocessedMessageTask() {
        JdbcSinkTask jdbcSinkTask = jdbcSinkTaskMap.get(ERROR_TOPIC);
        if (jdbcSinkTask == null) {
            jdbcSinkTask = new JdbcSinkTask();
            jdbcSinkTask.start(getErrorMessageJdbcSinkTaskConfiguration());
            jdbcSinkTaskMap.put(ERROR_TOPIC, jdbcSinkTask);
        }
        return jdbcSinkTask;
    }


    public void putUnprocessedRecords(Collection<SinkRecord> records) {
        getUnprocessedMessageTask().put(records);
    }

    public String getErrorTopic() {
        return ERROR_TOPIC;
    }

    public DatabaseType getDatabaseType() {
        return databaseType;
    }
}
