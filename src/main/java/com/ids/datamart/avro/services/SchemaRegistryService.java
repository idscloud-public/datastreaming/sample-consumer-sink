package com.ids.datamart.avro.services;

import com.ids.datamart.avro.transformations.DatabaseType;
import com.ids.datamart.avro.transformations.MessageTransformer;
import io.confluent.connect.jdbc.sink.metadata.SchemaPair;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import org.apache.kafka.connect.data.*;
import org.apache.kafka.connect.transforms.util.SchemaUtil;
import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Contains utility methods for schema conversions
 */
@Service
public class SchemaRegistryService {

    private static final String VALUE_SUBJECT = "-value";

    private static final String KEY_SUBJECT = "-key";

    private static final String PATH_DELIMITER = "/";

    @Value("${kafka.local.schema.repository:@null}")
    private String schemaFolder;

    @Autowired
    private SchemaRegistryClientProvider schemaRegistryClientProvider;

    @Autowired
    private MessageTransformer messageTransformer;

    private Map<String, ConvertedSchemaInfo> convertedSchemaMap = new HashMap<>();

    public static final  Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(SchemaRegistryService.class);

    /**
     * @return SchemaRegistryClient
     */
    private SchemaRegistryClient getSchemaRegistryClient() {
        return schemaRegistryClientProvider.getSchemaRegistryClient();
    }

    /**
     * Gets avro schema {@link org.apache.avro.Schema} from schema-registry
     *
     * @param subject
     * @return
     */
    public org.apache.avro.Schema getSchemaFromSchemaRegistry(String subject) throws RestClientException, IOException {
        org.apache.avro.Schema.Parser avroSchemaParser = new org.apache.avro.Schema.Parser();
        return avroSchemaParser.parse(getSchemaRegistryClient().getLatestSchemaMetadata(subject).getSchema());
    }

    /**
     * Verifies if date conversion is required
     *
     * @param schema input {@link org.apache.kafka.connect.data.Schema}
     * @return
     */
    public boolean isDateConversionRequired(org.apache.avro.Schema schema) {
        Map<String, Object> schemaProps = schema.getObjectProps();
        for (Map.Entry<String, Object> entry : schemaProps.entrySet()) {
            String key = entry.getKey();
            if (key.equals("logicalType") && schemaProps.get(key).equals("date") && schema.getType() == org.apache.avro.Schema.Type.LONG) {
                return true;
            }
        }
        return false;
    }


    /**
     * Creates new schema from input schema by updating input fields to {@link Date} fields
     *
     * @param schema         input {@link org.apache.kafka.connect.data.Schema}
     * @param fieldsToUpdate fields that require date conversion
     * @return
     */
    public Schema updateFieldsToLogicalDateSchema(Schema schema, List<String> fieldsToUpdate) {
        SchemaBuilder builder = SchemaUtil.copySchemaBasics(schema, SchemaBuilder.struct());
        Schema baseDateSchema = Date.builder().build();
        for (Field field : schema.fields()) {
            if (fieldsToUpdate.contains(field.name())) {
                Schema fieldSchema = field.schema();
                Schema dateSchema = new ConnectSchema(baseDateSchema.type(), fieldSchema.isOptional(), fieldSchema.defaultValue(), baseDateSchema.name(),
                        fieldSchema.version(), fieldSchema.doc());
                builder.field(field.name(), dateSchema);
            } else {
                builder.field(field.name(), field.schema());
            }
        }
        return builder.build();
    }


    /**
     * Converts latest avro schema of a topic's message value to {@link org.apache.kafka.connect.data.Schema} and stores in map
     *
     * @param topic  topic to read
     * @param schema original {@link org.apache.kafka.connect.data.Schema} schema of topic
     */
    private synchronized void updateConvertedSchemaInfoWithDateInformation(String topic, Schema schema) {
        String subject = topic + VALUE_SUBJECT;
        ConvertedSchemaInfo convertedSchemaInfo = new ConvertedSchemaInfo();
        org.apache.avro.Schema avroSchema = null;
        try {
            avroSchema = getSchemaFromSchemaRegistry(subject);
        } catch (RestClientException | IOException e) {
            if (schemaFolder != null) {
                String[] split = topic.split("\\.");
                int length = split.length;
                String tableName = split[length - 1];
                String localSchemaName = tableName + VALUE_SUBJECT;
                avroSchema = getAvroSchemaFromLocalRepository(localSchemaName);
            }
            if (avroSchema == null) {
                throw new NotFoundException(subject + " schema not found. ");
            }
        }
        convertedSchemaInfo.setAvroSchema(avroSchema);

        for (org.apache.avro.Schema.Field field : avroSchema.getFields()) {
            org.apache.avro.Schema fieldSchema = field.schema();
            if (fieldSchema.getType() == org.apache.avro.Schema.Type.UNION) {
                Iterator iterator = fieldSchema.getTypes().iterator();
                while (iterator.hasNext()) {
                    org.apache.avro.Schema unionSchema = (org.apache.avro.Schema) iterator.next();
                    if (isDateConversionRequired(unionSchema)) {
                        convertedSchemaInfo.addDateFieldForConversion(field.name());
                    }
                }
            } else {
                if (isDateConversionRequired(fieldSchema)) {
                    convertedSchemaInfo.addDateFieldForConversion(field.name());
                }
            }
        }

        if (convertedSchemaInfo.isDateConversionRequired()) {
            Schema updatedSchema = updateFieldsToLogicalDateSchema(schema, convertedSchemaInfo.getDateFieldsForCoversion());
            convertedSchemaInfo.setConvertedConnectSchema(updatedSchema);
        }

        convertedSchemaInfo.setVersion(schema.version());
        convertedSchemaMap.put(topic, convertedSchemaInfo);
    }

    /**
     * Updates date fields from Long to {@link org.apache.kafka.connect.data.Date}
     *
     * @param originalValue
     * @param convertedSchemaInfo object that holds conversion information
     * @return
     */
    private Object convertLongToLogicalDate(Struct originalValue, ConvertedSchemaInfo convertedSchemaInfo) {
        Schema updatedSchema = convertedSchemaInfo.getConvertedConnectSchema();
        Struct updatedValue = new Struct(updatedSchema);
        for (Field field : updatedSchema.fields()) {
            Object fieldValue = originalValue.get(field.name());
            if (convertedSchemaInfo.getDateFieldsForCoversion().contains(field.name())) {
                Long dateInINT64Format = originalValue.getInt64(field.name());
                if (dateInINT64Format != null) {
                    java.util.Date timestamp = Timestamp.toLogical(Timestamp.builder().build(), dateInINT64Format);
                    fieldValue = timestamp;
                }
            }
            updatedValue.put(field.name(), fieldValue);
        }
        return updatedValue;
    }

    /**
     * Converts data in byte[] format to kafka-connect format
     *
     * @param topic kafka topic to read data from
     * @param data  data to convert
     * @param isKey boolean value to tell whether data is kafka key or kafka value
     * @return ScehmaAndValue object containing connect schema and its value
     */
    public SchemaAndValue getSchemaAndValue(String topic, byte[] data, boolean isKey) {
        if (isKey) {
            return schemaRegistryClientProvider.getKeyAvroConvertor().toConnectData(topic, data);
        } else {
            SchemaAndValue schemaAndValue = schemaRegistryClientProvider.getValueAvroConvertor().toConnectData(topic, data);
            ConvertedSchemaInfo convertedSchemaInfo = convertedSchemaMap.get(topic);
            if (convertedSchemaInfo == null || convertedSchemaInfo.getVersion() != schemaAndValue.schema().version()) {
                updateConvertedSchemaInfoWithDateInformation(topic, schemaAndValue.schema());
                convertedSchemaInfo = convertedSchemaMap.get(topic);
            }
            if (convertedSchemaInfo.isDateConversionRequired()) {
                schemaAndValue = new SchemaAndValue(convertedSchemaInfo.getConvertedConnectSchema(), convertLongToLogicalDate((Struct) schemaAndValue.value(), convertedSchemaInfo));
            }
            return schemaAndValue;
        }
    }

    /**
     * Returns avro schema stored in local schema repository
     *
     * @return
     */
    private org.apache.avro.Schema getAvroSchemaFromLocalRepository(String subject) {
        if (schemaFolder != null) {
            try {
                return new org.apache.avro.Schema.Parser().parse(new File(schemaFolder + PATH_DELIMITER + subject + ".avsc"));
            } catch (IOException e) {
                return null;
            }
        }

        return null;
    }


    private org.apache.avro.Schema getAvroSchema(String topic, String tableName, String keyOrValue) {
        org.apache.avro.Schema avroSchema = null;
        String subject = topic + keyOrValue;
        try {
            avroSchema = getSchemaFromSchemaRegistry(subject);
        } catch (RestClientException | IOException e) {
            if (schemaFolder != null) {
                String localSchemaName = tableName + keyOrValue;
                avroSchema = getAvroSchemaFromLocalRepository(localSchemaName);
            }
            if (avroSchema == null) {
                throw new NotFoundException(subject + " schema not found. ");
            }
        }

        return avroSchema;
    }

    /**
     * Returns {@link SchemaPair} for the input topic
     *
     * @param topic name of the topic
     * @return key schema and value schema pair of the topic
     */
    public SchemaPair getSchemaPair(String topic, DatabaseType databaseType, String tableName) {
        org.apache.avro.Schema avroValueSchema = getAvroSchema(topic, tableName, VALUE_SUBJECT);
        Schema valueSchema = schemaRegistryClientProvider.getAvroData().toConnectSchema(avroValueSchema);
        if (valueSchema != null) {
            updateConvertedSchemaInfoWithDateInformation(topic, valueSchema);
            if (convertedSchemaMap.get(topic).isDateConversionRequired()) {
                valueSchema = convertedSchemaMap.get(topic).getConvertedConnectSchema();
            }
            valueSchema = messageTransformer.updateSchemaWithTransformation(tableName, valueSchema, databaseType);
            logger.info(String.format("Value Schema for topic %s is %s", topic, valueSchema));
        } else {
            logger.error(String.format("Could not find value Schema for topic %s", topic));
        }

        org.apache.avro.Schema avroKeySchema = getAvroSchema(topic, tableName, KEY_SUBJECT);
        Schema keySchema = schemaRegistryClientProvider.getAvroData().toConnectSchema(avroKeySchema);
        if (keySchema != null) {
            keySchema = messageTransformer.updateSchemaWithTransformation(tableName, keySchema, databaseType);
            logger.info(String.format("Key Schema for topic %s is %s", topic, keySchema));
        } else {
            logger.error(String.format("Could not find key Schema for topic %s", topic));
        }

        return new SchemaPair(keySchema, valueSchema);
    }
}