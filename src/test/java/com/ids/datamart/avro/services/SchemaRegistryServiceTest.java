package com.ids.datamart.avro.services;

import io.confluent.connect.avro.AvroConverter;
import io.confluent.kafka.schemaregistry.client.SchemaMetadata;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import org.apache.kafka.connect.data.ConnectSchema;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.SchemaAndValue;
import org.apache.kafka.connect.data.SchemaBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class SchemaRegistryServiceTest {

    @Mock
    SchemaRegistryClientProvider schemaRegistryClientProvider;

    @Mock
    SchemaRegistryClient schemaRegistryClient;

    @InjectMocks
    SchemaRegistryService schemaRegistryService;

    private String avroSchemaString = "{\"type\":\"record\",\"name\":\"cs_master_nf\",\"namespace\":\"com.ids.datamart.avro\",\"fields\":[{\"name\":\"id\",\"type\":\"long\"},{\"name\":\"parent_co_county\",\"type\":[\"null\",\"string\"]},{\"name\":\"parent_co_country\",\"type\":[\"null\",\"string\"]},{\"name\":\"landlord_county\",\"type\":[\"null\",\"string\"]},{\"name\":\"landlord_country\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_high_aggr_code\",\"type\":[\"null\",\"long\"]},{\"name\":\"cust_exposure\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"dealer_cust_num\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_parent_code\",\"type\":[\"null\",\"long\"]},{\"name\":\"parent_co_dba\",\"type\":[\"null\",\"string\"]},{\"name\":\"business_state\",\"type\":[\"null\",\"string\"]},{\"name\":\"business_tax_id\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_risk_grade\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_reclass_status\",\"type\":[\"null\",\"int\"]},{\"name\":\"cust_reclass_comments\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_reclass_personnel\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_reclass_date\",\"type\":[\"null\",{\"type\":\"long\",\"logicalType\":\"date\"}]},{\"name\":\"cust_ap_vendor\",\"type\":[\"null\",\"bytes\"]},{\"name\":\"pref_cust\",\"type\":[\"null\",\"string\"]},{\"name\":\"pref_credit_limit\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"effective_date\",\"type\":[\"null\",{\"type\":\"long\",\"logicalType\":\"date\"}]},{\"name\":\"pref_cust_card\",\"type\":[\"null\",\"string\"]},{\"name\":\"credit_analyst\",\"type\":[\"null\",\"string\"]},{\"name\":\"pref_credit_used\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"third_party_guar\",\"type\":\"int\"},{\"name\":\"db_rating_code\",\"type\":[\"null\",\"string\"]},{\"name\":\"db_rating\",\"type\":[\"null\",\"string\"]},{\"name\":\"business\",\"type\":[\"null\",\"string\"]},{\"name\":\"begin_bus_yr\",\"type\":[\"null\",\"string\"]},{\"name\":\"business_type\",\"type\":[\"null\",\"string\"]},{\"name\":\"business_phone\",\"type\":[\"null\",\"string\"]},{\"name\":\"cur_db_rating\",\"type\":[\"null\",\"string\"]},{\"name\":\"card_type\",\"type\":[\"null\",\"string\"]},{\"name\":\"years_in_business\",\"type\":[\"null\",\"string\"]},{\"name\":\"premises\",\"type\":[\"null\",\"string\"]},{\"name\":\"parent_co_name\",\"type\":[\"null\",\"string\"]},{\"name\":\"parent_co_add1\",\"type\":[\"null\",\"string\"]},{\"name\":\"parent_co_add2\",\"type\":[\"null\",\"string\"]},{\"name\":\"parent_co_add3\",\"type\":[\"null\",\"string\"]},{\"name\":\"parent_co_city\",\"type\":[\"null\",\"string\"]},{\"name\":\"parent_co_state\",\"type\":[\"null\",\"string\"]},{\"name\":\"parent_co_zip\",\"type\":[\"null\",\"string\"]},{\"name\":\"parent_co_phone\",\"type\":[\"null\",\"string\"]},{\"name\":\"landlord_name\",\"type\":[\"null\",\"string\"]},{\"name\":\"landlord_add1\",\"type\":[\"null\",\"string\"]},{\"name\":\"landlord_add2\",\"type\":[\"null\",\"string\"]},{\"name\":\"landlord_add3\",\"type\":[\"null\",\"string\"]},{\"name\":\"landlord_city\",\"type\":[\"null\",\"string\"]},{\"name\":\"landlord_state\",\"type\":[\"null\",\"string\"]},{\"name\":\"landlord_zip\",\"type\":[\"null\",\"string\"]},{\"name\":\"landlord_phone\",\"type\":[\"null\",\"string\"]},{\"name\":\"spec_instr_time_ent\",\"type\":[\"null\",\"string\"]},{\"name\":\"spec_instr_comment\",\"type\":[\"null\",\"string\"]},{\"name\":\"spec_instr_analyst\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_prop_status\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_tax_exempt_code\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_vendor\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_file_fhut\",\"type\":[\"null\",\"int\"]},{\"name\":\"cust_excl_from_web\",\"type\":[\"null\",\"int\"]},{\"name\":\"outstanding_balance\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"business_desc\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_high_credit\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"cust_credit_limit\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"cust_net_invest\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"cust_credit_code\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_country_code\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_bulk_dd\",\"type\":[\"null\",\"int\"]},{\"name\":\"cust_publicly_traded\",\"type\":[\"null\",\"int\"]},{\"name\":\"cust_existing_customer\",\"type\":[\"null\",\"int\"]},{\"name\":\"cust_user_exposure\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"cust_extract_seq_nbr\",\"type\":[\"null\",\"long\"]},{\"name\":\"cust_risk_chng_user\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_reference_num\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_ccb_branch\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_solicit\",\"type\":[\"null\",\"int\"]},{\"name\":\"cust_ref_trans_id\",\"type\":[\"null\",\"long\"]},{\"name\":\"cust_blend_net_inv\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"cust_ind_blend_net_inv\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"cust_usg_exp_id\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_locked_agent\",\"type\":[\"null\",\"string\"]},{\"name\":\"cust_ni_less_accr_int\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"cust_tot_credit_exp\",\"type\":[\"null\",{\"type\":\"bytes\",\"logicalType\":\"decimal\",\"precision\":13,\"scale\":2}]},{\"name\":\"source_system\",\"type\":[\"null\",\"string\"],\"doc\":\"IDS source system that produced message\",\"default\":null},{\"name\":\"source_version\",\"type\":[\"null\",\"string\"],\"doc\":\"Version of IDS source system that produced message\",\"default\":null},{\"name\":\"ids_stream_version\",\"type\":[\"null\",\"string\"],\"doc\":\"Version of IDS streams that processed message\",\"default\":null},{\"name\":\"breadcrumb_id\",\"type\":[\"null\",\"string\"],\"doc\":\"ID used to track message from CDC\",\"default\":null},{\"name\":\"cdc_crefate_timestamp\",\"type\":[\"null\",{\"type\":\"long\",\"logicalType\":\"timestamp-millis\"}],\"doc\":\"Timestamp when CDC record was created in database\",\"default\":null}]}";

    @Test
    public void testUpdateFieldsToLogicalDateSchema() {
        SchemaRegistryService schemaRegistryService = new SchemaRegistryService();
        SchemaBuilder schemaBuilder = new SchemaBuilder(Schema.Type.STRUCT);
        schemaBuilder.field("id", SchemaBuilder.INT64_SCHEMA);
        schemaBuilder.field("last_exposure_date", SchemaBuilder.INT64_SCHEMA);
        schemaBuilder.field("cust_reclass_date", SchemaBuilder.INT64_SCHEMA);
        schemaBuilder.field("parent_co_county", SchemaBuilder.STRING_SCHEMA);
        schemaBuilder.field("landlord_county", SchemaBuilder.STRING_SCHEMA);
        schemaBuilder.field("business", SchemaBuilder.STRING_SCHEMA);
        schemaBuilder.name("testSchema");
        schemaBuilder.doc("schema for unit testing");
        schemaBuilder.version(1);
        List<String> fieldsToUpdate = new ArrayList<>();
        fieldsToUpdate.add("last_exposure_date");
        fieldsToUpdate.add("cust_reclass_date");

        Schema resultSchema = schemaRegistryService.updateFieldsToLogicalDateSchema(schemaBuilder.build(), fieldsToUpdate);

        assertEquals("testSchema", resultSchema.name());
        assertEquals("schema for unit testing", resultSchema.doc());
        assertEquals(1, resultSchema.version());
        assertEquals(Schema.INT64_SCHEMA, resultSchema.field("id").schema());
        assertEquals(Schema.STRING_SCHEMA, resultSchema.field("parent_co_county").schema());
        assertEquals(Schema.STRING_SCHEMA, resultSchema.field("landlord_county").schema());
        assertEquals(Schema.STRING_SCHEMA, resultSchema.field("business").schema());
        assertEquals("org.apache.kafka.connect.data.Date", resultSchema.field("last_exposure_date").schema().name());
        assertEquals("org.apache.kafka.connect.data.Date", resultSchema.field("cust_reclass_date").schema().name());
    }

    @Test
    public void testGetSchemaFromSchemaRegistry() {
        SchemaMetadata schemaMetadata = new SchemaMetadata(1, 1, avroSchemaString);
        try {
            Mockito.when(schemaRegistryClient.getLatestSchemaMetadata(anyString())).thenReturn(schemaMetadata);
            Mockito.when(schemaRegistryClientProvider.getSchemaRegistryClient()).thenReturn(schemaRegistryClient);
            org.apache.avro.Schema schema = schemaRegistryService.getSchemaFromSchemaRegistry("testTopic");

            org.apache.avro.Schema.Field field = schema.getField("id");
            assertEquals(org.apache.avro.Schema.Type.LONG, field.schema().getType());
            assertEquals(0, field.pos());

            field = schema.getField("cust_exposure");
            assertEquals(org.apache.avro.Schema.Type.UNION, field.schema().getType());
            assertEquals(6, field.pos());

            field = schema.getField("effective_date");
            assertEquals(org.apache.avro.Schema.Type.UNION, field.schema().getType());
            assertEquals(20, field.pos());

            List<org.apache.avro.Schema> avroSchemas = field.schema().getTypes();
            assertEquals(org.apache.avro.Schema.Type.NULL, avroSchemas.get(0).getType());
            assertEquals(org.apache.avro.Schema.Type.LONG, avroSchemas.get(1).getType());
            assertEquals("date", avroSchemas.get(1).getProp("logicalType"));
        } catch (Exception e) {

        }
    }

    @Test
    public void testIsDateConversionRequired() {
        org.apache.avro.Schema.Parser parser = new org.apache.avro.Schema.Parser();
        SchemaMetadata schemaMetadata = new SchemaMetadata(1, 1, avroSchemaString);
        org.apache.avro.Schema schema = parser.parse(schemaMetadata.getSchema());

        org.apache.avro.Schema.Field field = schema.getField("effective_date");
        List<org.apache.avro.Schema> avroSchemas = field.schema().getTypes();
        assertTrue(schemaRegistryService.isDateConversionRequired(avroSchemas.get(1)));
        assertFalse(schemaRegistryService.isDateConversionRequired(avroSchemas.get(0)));

        field = schema.getField("cust_reclass_date");
        avroSchemas = field.schema().getTypes();
        assertTrue(schemaRegistryService.isDateConversionRequired(avroSchemas.get(1)));
        assertFalse(schemaRegistryService.isDateConversionRequired(avroSchemas.get(0)));
    }

    @Test
    public void testGetSchemaAndValueForMessageKey() throws RestClientException, IOException {
        AvroConverter keyAvroConvertor = mock(AvroConverter.class);
        SchemaAndValue schemaAndValue = new SchemaAndValue(new ConnectSchema(Schema.Type.STRUCT), null);
        Mockito.when(keyAvroConvertor.toConnectData(anyString(), any())).thenReturn(schemaAndValue);
        Mockito.when(schemaRegistryClientProvider.getKeyAvroConvertor()).thenReturn(keyAvroConvertor);

        SchemaAndValue result = schemaRegistryService.getSchemaAndValue("testTopic", null, true);
        assertEquals(schemaAndValue, result);
    }

    @Test
    public void testGetSchemaAndValueForMessageValue() {
        SchemaMetadata schemaMetadata = new SchemaMetadata(1, 1, avroSchemaString);

        AvroConverter valueAvroConvertor = mock(AvroConverter.class);
        SchemaAndValue schemaAndValue = new SchemaAndValue(new ConnectSchema(Schema.Type.STRUCT, false, null, "testSchema", 1, "this is test schema"), null);

        try {
            Mockito.when(schemaRegistryClient.getLatestSchemaMetadata(anyString())).thenReturn(schemaMetadata);
            Mockito.when(valueAvroConvertor.toConnectData(anyString(), any())).thenReturn(schemaAndValue);
            Mockito.when(schemaRegistryClientProvider.getValueAvroConvertor()).thenReturn(valueAvroConvertor);
            Mockito.when(schemaRegistryClientProvider.getSchemaRegistryClient()).thenReturn(schemaRegistryClient);

            SchemaAndValue result = schemaRegistryService.getSchemaAndValue("testTopic", null, false);
            assertEquals(schemaAndValue.schema(), result.schema());
        } catch (Exception e) {

        }
    }


}
