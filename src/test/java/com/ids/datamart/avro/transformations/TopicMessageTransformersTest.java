package com.ids.datamart.avro.transformations;

import org.apache.kafka.connect.connector.ConnectRecord;
import org.apache.kafka.connect.data.*;
import org.apache.kafka.connect.sink.SinkRecord;
import org.apache.kafka.connect.transforms.ReplaceField;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class TopicMessageTransformersTest {

  @Test
  public void testExecuteTransformationsWhenTransformationsEmpty() {
    TopicMessageTransformers topicMessageTransformers = new TopicMessageTransformers();
    topicMessageTransformers.setTopicName("testtopic");

    topicMessageTransformers.setTransformations(Collections.emptyList());
    SinkRecord testSinkRecord = new SinkRecord("testtopic", 1, Schema.STRING_SCHEMA, "message-key", Schema.INT32_SCHEMA, 1234, 2);
    ConnectRecord<SinkRecord> sinkRecord = topicMessageTransformers.executeTransformations(testSinkRecord, DatabaseType.SQL_SERVER);
    assertEquals(testSinkRecord, sinkRecord);
  }

  @Test
  public void testUnsupportedExceptionOnWrongTopic() {
    TopicMessageTransformers topicMessageTransformers = new TopicMessageTransformers();
    topicMessageTransformers.setTopicName("testtopic1");

    SinkRecord testSinkRecord = new SinkRecord("testTopicName", 1, Schema.STRING_SCHEMA, "message-key", Schema.INT32_SCHEMA, 1234, 2);
    Exception exception = assertThrows(UnsupportedOperationException.class, () -> {
      topicMessageTransformers.executeTransformations(testSinkRecord, DatabaseType.SQL_SERVER);
    });
    String expectedMessage = "These transformations are not allowed for testTopicName topic";
    assertEquals(expectedMessage, exception.getMessage());
  }

  @Test
  public void testExecuteTransformations() {
    TopicMessageTransformers topicMessageTransformers = new TopicMessageTransformers();
    topicMessageTransformers.setTopicName("testTopicName");

    ReplaceField<SinkRecord> recordReplaceField = new ReplaceField.Value<>();
    Map<String, String> config = new HashMap<>();
    config.put("exclude", "integer_value");
    recordReplaceField.configure(config);
    MessageTransformation messageTransformation = new MessageTransformation(recordReplaceField, DatabaseType.SQL_SERVER, config);
    topicMessageTransformers.setTransformations(Collections.singletonList(messageTransformation));

    SchemaBuilder schemaBuilder = SchemaBuilder.struct();
    schemaBuilder.field("char_value", Schema.STRING_SCHEMA);
    schemaBuilder.field("integer_value", Schema.INT32_SCHEMA);
    schemaBuilder.field("boolean_value", Schema.BOOLEAN_SCHEMA);
    Schema schema = schemaBuilder.build();
    Struct value = new Struct(schema);
    value.put("char_value", "char_val");
    value.put("integer_value", 12345);
    value.put("boolean_value", false);
    SinkRecord testSinkRecord = new SinkRecord("testTopicName", 1, Schema.STRING_SCHEMA, "message-key", schema, value, 2);

    SinkRecord result = (SinkRecord) topicMessageTransformers.executeTransformations(testSinkRecord, DatabaseType.POSTGRESQL);
    for (Field field : result.valueSchema().fields()) {
      assertTrue(field.name().equals("char_value") || field.name().equals("integer_value") || field.name().equals("boolean_value"));
    }

    result = (SinkRecord) topicMessageTransformers.executeTransformations(testSinkRecord, DatabaseType.SQL_SERVER);
    for (Field field : result.valueSchema().fields()) {
      assertNotEquals("integer_value", field.name());
    }
    assertEquals(2, result.valueSchema().fields().size());
  }
}
